//
//  SignUpModel.swift
//  TookLee
//
//  Created by Nanak on 13/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import Foundation

class SignUpModel{
    
    var fieldName: String?
    var fieldValue: String?
    var countryDetail = CountryDataModel()
    
    init(with f_name: String, with f_value: String?) {
        
        self.fieldName = f_name
        self.fieldValue = f_value
    }
}

class CountryDataModel {
    
    var countryName: String?
    var city: String?
    var postalCode: String?
    
    init() {
        
    }
}
