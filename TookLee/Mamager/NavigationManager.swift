//
//  NavigationManager.swift
//  TookLee
//
//  Created by Nanak on 23/03/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift

struct NavigationManager {
    
    
    static func gotoLoginPage(){
        
        let mainViewController = LoginSignupOptionVC.instantiate(fromAppStoryboard: .Main)
        sharedAppDelegate.parentNavigationController = UINavigationController(rootViewController: mainViewController)
        sharedAppDelegate.parentNavigationController.isNavigationBarHidden = true
        sharedAppDelegate.window?.rootViewController = sharedAppDelegate.parentNavigationController
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func moveToLoginWithPhone(_ state : EnterPhoneState = .signUp) {
        
        let obj = LoginWithPhoneVC.instantiate(fromAppStoryboard: .Main)
        obj.enterPhoneState = state
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
        
    }
    
    static func moveToCountryDetail() {
        
        let obj = CountryCodeVC.instantiate(fromAppStoryboard: .Main)
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
        
    }


    static func moveToCreateProfile() {
        
        let obj = PersonalDetailVC.instantiate(fromAppStoryboard: .Main)
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
        
    }

    
    static func gotoHome(){
        
        let leftMenuViewController = SideMenuVC.instantiate(fromAppStoryboard: .Home)
        let mainViewController = LandingVC.instantiate(fromAppStoryboard: .Home)
        sharedAppDelegate.parentNavigationController = UINavigationController(rootViewController: mainViewController)
        let leftslideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftMenuViewController)
        sharedAppDelegate.parentNavigationController.isNavigationBarHidden = true
//        sharedAppDelegate.parentNavigationController.automaticallyAdjustsScrollViewInsets = false
        leftslideMenuController.mainViewController = sharedAppDelegate.parentNavigationController
//        leftslideMenuController.automaticallyAdjustsScrollViewInsets = true
        sharedAppDelegate.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        sharedAppDelegate.window?.rootViewController = leftslideMenuController
        sharedAppDelegate.window?.makeKeyAndVisible()

    }

    static func moveToVarifyPhoneOtp(otpState: EnterPhoneState = .signIn,  token : String = "", code: String = "", phone: String = "") {
        
        let verifyOtp = OTPViewController.instantiate(fromAppStoryboard: .Main)
        verifyOtp.code = code
        verifyOtp.mobileNumberText = phone
        verifyOtp.token = token
        verifyOtp.otpState = otpState
        sharedAppDelegate.parentNavigationController.pushViewController(verifyOtp, animated: true)
    }

    static func moveToVarifyEmailOtp(otpState: EnterEmailState = .varifyEmail,  token : String = "", email: String = "") {
        
        let verifyOtp = EmailOTPVerifyVC.instantiate(fromAppStoryboard: .Main)
        verifyOtp.email = email
        verifyOtp.token = token
        verifyOtp.otpVarifyState = otpState
        sharedAppDelegate.parentNavigationController.pushViewController(verifyOtp, animated: true)
    }

    static func moveToEnterEmail(with emailState : EnterEmailState = .varifyEmail) {
        
        let verifyOtp = ChangeEmailVC.instantiate(fromAppStoryboard: .Main)
            verifyOtp.emailState = emailState
        sharedAppDelegate.parentNavigationController.pushViewController(verifyOtp, animated: true)
    }

    static func moveToWelcomeScene(isLogin: Bool) {
        
        let welcomeScene = WelcomeVC.instantiate(fromAppStoryboard: .Main)
        welcomeScene.isLogin = isLogin
        sharedAppDelegate.parentNavigationController.pushViewController(welcomeScene, animated: true)
    }

    
    static func gotoPersonalDetail(from viewController: UIViewController , detailType : PersonalDetailType  = .email, detail : JSONDictionary = [:]) {
            let obj = PersonalDetailVC.instantiate(fromAppStoryboard: .Main)
            obj.detailType = detailType
            obj.personalDetail = detail
            sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
        }
    
    static func movetoSelectPaymentType() {
        let obj = SelectPaymentTypeVC.instantiate(fromAppStoryboard: .Main)
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
    }

    
    static func moveToProfile() {
        let obj = ProfileVC.instantiate(fromAppStoryboard: .Profile)
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
    }

    static func moveToEditProfile(with editState : EditProfileState = .name) {
        
        let obj = EditProfileVC.instantiate(fromAppStoryboard: .Profile)
        obj.editState = editState
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
        
    }

    static func moveToTerms() {
        
        let obj = TermsServicesVC.instantiate(fromAppStoryboard: .Main)
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)
    }

    
}
