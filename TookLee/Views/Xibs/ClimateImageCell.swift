//
//  ClimateImageCell.swift
//  TookLee
//
//  Created by Nanak on 11/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class ClimateImageCell: UITableViewCell {

    @IBOutlet weak var climateImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
