//
//  HomeOfferRideCell.swift
//  TookLee
//
//  Created by Nanak on 11/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class HomeOfferRideCell: UITableViewCell {

    
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var offerARideLbl: UILabel!
    @IBOutlet weak var descrptionLbl: UILabel!
    @IBOutlet weak var getStartedBtn: UIButton!
    
    let offerARide = "Drive with tooklee and share fuel cost\n\n make the best use of those unused spaces and protect climate\n\n Meet new friends and enjoy ride."

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.descrptionLbl.text = offerARide
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
