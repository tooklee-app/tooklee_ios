//
//  HomeClimateCell.swift
//  TookLee
//
//  Created by Nanak on 11/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class HomeClimateCell: UITableViewCell {

    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var protectClimateLbl: UILabel!
    @IBOutlet weak var descrptionLbl: UILabel!
    @IBOutlet weak var getStartedBtn: UIButton!
    
    var climate = "Each car is producing 1.4 metric ton of carbon every year\n\n Sharing rides results is using fewer cars\n\n Less Traffic - Less Carbon - Better Climate"
    override func awakeFromNib() {
        super.awakeFromNib()
        self.descrptionLbl.text = climate
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
