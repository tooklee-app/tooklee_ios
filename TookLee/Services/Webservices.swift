//
//  Webservices.swift
//  StarterProj
//
//  Created by Gurdeep on 16/12/16.
//  Copyright © 2016 Gurdeep. All rights reserved.
//

import Foundation
import SwiftyJSON

internal typealias successClosure = (_ success : Bool, _ errorMessage: String, _ data: JSON) -> Void
internal typealias successClosureWithCode = (_ code : Int, _ errorMessage: String, _ data: JSON) -> Void

internal typealias successArrayClosure = (_ success : Bool, _ errorMessage: String, _ data: [JSON]) -> Void

enum NetworkResponse {
    
    case value(JSON)
    case error(Error)
}

enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}

extension WebServices {
    
    static func signInAPI(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .login, parameters: parameters, loader: true, success: { (json) in
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
            
        }) { (e) in
            failure(e)
            
        }
        
    }
    
    
    static func signUpAPI(parameters : JSONDictionary, webServiceSuccess : @escaping successClosureWithCode, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .signup, parameters: parameters, loader: true, success: { (json) in
            checkSessionWithCode(with: json, completionBlock: { (code, msg, data) in
                webServiceSuccess(code,msg,data)
            })
            
        }) { (e) in
            failure(e)
            
        }
        
    }
    
    
    static func sendOTPOnEmailAPI(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .sendEmailOTP, parameters: parameters, loader: true, success: { (json) -> () in
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func sendOTPOnPhoneAPI(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .sendPhoneOTP, parameters: parameters, loader: true, success: { (json) -> () in
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    
    static func verifyOTP(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .varifyOTP, parameters: parameters,  loader: true, success: { (json) -> () in
            
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func updatePhone(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .update_phone, parameters: parameters,  loader: true, success: { (json) -> () in
            
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    
    static func updateProfile(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .update_profile, parameters: parameters,  loader: true, success: { (json) -> () in
            
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    
    static func createSession(parameters : JSONDictionary, webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .create_session, parameters: parameters,  loader: true, success: { (json) -> () in
            
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    
    static func createProfileapi(parameters : JSONDictionary ,image: [String : UIImage]? , webServiceSuccess : @escaping successClosure, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POSTWithImage(endPoint: .createProfile, parameters: parameters, image: image,loader: true, success: { (json) in
            checkSession(with: json, completionBlock: { (isSuccess, msg, data) in
                webServiceSuccess(isSuccess,msg,data)
            })
            
        }) { (err) in
            failure(err)
        }
    }
    
}


//MARK:- Error Codes
//==================

struct error_codes {
    
    static let success = 200
    static let alreadyRegistered = 201
    
}


func checkSessionWithCode(with response : JSON , completionBlock: successClosureWithCode){
    
    let message = response["msg"].stringValue
    let code = response["code"].intValue
    let result = response["data"]
    
    completionBlock(code, message, result)

}


func checkSession(with response : JSON , completionBlock: successClosure){
    
    let message = response["msg"].stringValue
    let code = response["code"].intValue
    
    if code == error_codes.success{
        
        let result = response["data"]
        
        completionBlock(true, message, result)
        
    }else{
        
        completionBlock(false, message, [:])
    }
}


func checkSessionWithArrayData(with response : JSON , completionBlock: successArrayClosure){
    
    let message = response["msg"].stringValue
    let code = response["code"].intValue
    
    if code == error_codes.success{
        
        let result = response["data"].arrayValue
        
        completionBlock(true, message, result)
        
    }else{
        
        completionBlock(false, message, [[:]])
    }
}

