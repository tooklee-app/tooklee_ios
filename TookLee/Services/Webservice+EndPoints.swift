//
//  Webservice+EndPoints.swift
//  StarterProj
//
//  Created by Gurdeep on 06/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation

let BASE_URL = "http://52.15.165.237:3000/"

extension WebServices {

    enum EndPoint : String {
       
        case signup = "auth/register"
        case varifyOTP = "auth/verify-otp"
        case login = "auth"
        case createProfile = "create-profile"
        case sendEmailOTP = "auth/send-email-otp"
        case sendPhoneOTP = "auth/send-phone-otp"
        case update_phone = "auth/update-phone"
        case create_session = "auth/create-session"
        case update_profile = "profile/update-profile"
        var path : String {
            
            let url = BASE_URL
            
            return url + self.rawValue
            
        }
    }
}


