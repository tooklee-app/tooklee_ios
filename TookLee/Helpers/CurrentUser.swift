//
//  CurrentUser.swift
//  TookLee
//
//  Created by Nanak on 23/03/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import Foundation


class CurrentUser {
    
    
    static var accessToken : String? {
        return AppUserDefaults.value(forKey: .Accesstoken).string
    }
    static var first_name : String? {
        return AppUserDefaults.value(forKey: .firstName).string
    }
    static var last_name : String? {
        return AppUserDefaults.value(forKey: .lastName).string
    }
    static var profile_pic : String? {
        return AppUserDefaults.value(forKey: .userImage).string
    }
    static var email : String? {
        return AppUserDefaults.value(forKey: .email).string
    }
    static var country_code : String? {
        return AppUserDefaults.value(forKey: .countryCode).string
    }
    static var account_status : Bool? {
        return AppUserDefaults.value(forKey: .account_status).bool
    }

    static var isEmailVerifiedd : Bool? {
        return AppUserDefaults.value(forKey: .is_email_verified).bool
    }

    static var phone : String? {
        return AppUserDefaults.value(forKey: .phone).string
    }
    static var dob : String? {
        return AppUserDefaults.value(forKey: .dob).string
    }
    static var otpVarified : Bool? {
        return AppUserDefaults.value(forKey: .otpVarified).bool
    }

    
    static var isLoggedIn : Bool{
        
        guard let account_status = self.account_status, let is_email_verified = self.isEmailVerifiedd , account_status , is_email_verified else{return false}
        
        return true
    }
    
}
