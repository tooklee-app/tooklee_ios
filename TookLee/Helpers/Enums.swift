//
//  Enums.swift
//  TookLee
//
//  Created by Nanak on 25/03/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import Foundation


enum PersonalDetailType {
    case email
    case facebook
    case gmail
}


enum EnterPhoneState {

    case signIn
    case signUp
    case changePhone

}

enum EnterEmailState {
    
    case changePhone
    case varifyEmail
    
}

enum EditProfileState {
    
    case name
    case email
    case phone
    case dob

}



struct DateFormate {
    
    static let utcDateWithTime = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let dateWithTime = "dd-MM-yyyy, hh:mm a"
    static let dateOnly = "MM.dd.yyyy"
    static let timeOnly = "hh:mm a"
}

struct TimeZoneString {
    
    static let UTC = "UTC"
    static let SGT = "SGT"
}


extension String{
    
    func convertTimeWithTimeZone(_ timeZome: String = TimeZoneString.UTC, formate: String) -> String{
        
        if self.isEmpty{
            
            return ""
            
        }else{
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = TimeZone(abbreviation: timeZome)
            
            dateFormatter.dateFormat = DateFormate.utcDateWithTime
            
            let date1 = dateFormatter.date(from: self)
            
            dateFormatter.dateFormat = formate
            
            dateFormatter.timeZone = TimeZone.autoupdatingCurrent
            
            dateFormatter.locale = Locale.current
            
            let strDate = dateFormatter.string(from: date1!)
            
            return strDate
            
        }
        
    }
}
