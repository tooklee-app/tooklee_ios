//
//  AppFonts.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit


enum AppFonts : String {
    
    case OpenSans_Bold = "OpenSans-Bold"
    case OpenSans_BoldItalic = "OpenSans-BoldItalic"
    case OpenSans_ExtraBold = "OpenSans-ExtraBold"
    case OpenSans_ExtraBoldItalic = "OpenSans-ExtraBoldItalic"
    case OpenSans_LightItalic = "OpenSans-LightItalic"
    case OpenSans_Italic = "OpenSans-Italic"
    case OpenSans = "OpenSans-Light"
    case OpenSans_Regular = "OpenSans-Regular"
    case OpenSans_Semibold = "OpenSans-Semibold"
    case OpenSans_SemiboldItalic = "OpenSans-SemiboldItalic"
}

extension AppFonts {
    
    func withSize(_ fontSize: CGFloat) -> UIFont {
        
        return UIFont(name: self.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    func withDefaultSize() -> UIFont {
        
        return UIFont(name: self.rawValue, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
    }
    
}

// USAGE : let font = AppFonts.Helvetica.withSize(13.0)
// USAGE : let font = AppFonts.Helvetica.withDefaultSize()
