//
//  AppColors.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit

struct AppColors {
    
    
    

   // static let themeColor               = UIColor(r: 14, g: 185, b: 203,alpha: 1)
//    static let lightBlueColor           = UIColor(r: 9, g: 185, b: 203, alpha: 0.4)
    static let lightBlueColor           = UIColor(r: 9, g: 208, b: 229, alpha: 0.62)

    static let bottomTrackViewColor     = UIColor(r: 4, g: 160, b: 174 ,alpha: 1)
    static let lightBlackColor          = UIColor(r: 0, g: 0, b: 0, alpha: 0.65)
//    static let btnTextGreenLightColor   = UIColor(r: 10, g: 161, b: 177,alpha: 1)
    static let btnTextGreenLightColor   = UIColor(r: 21, g: 172, b: 188,alpha: 1)

    static let btnBackGroundDarkColor   = UIColor(r: 0, g: 104, b: 115,alpha: 1)

    static let lightSkyBlueColor              = UIColor(r: 220, g: 240, b: 244,alpha: 1)
    static let greenishBlueColor        = UIColor(r: 110, g: 198, b: 207,alpha: 1)
    
    static let parrotGreenColor         = UIColor(r: 91, g: 216, b: 67,alpha: 1)
    static let blueColor                = UIColor(r: 98, g: 67, b: 216,alpha: 1)
    static let orangeColor              = UIColor(r: 91, g: 216, b: 67,alpha: 1)
    static let pinkColor                = UIColor(r: 216, g: 67, b: 204,alpha: 1)
   // static let yelowColor             = UIColor(r: 83, g: 204, b: 205 ,alpha: 1)
    static let ylwColor                 = UIColor(r: 255, g: 198, b: 0,alpha: 1)
    static let skyBlueColor             = UIColor(r: 83, g: 204, b: 205, alpha: 1)
    //======================================================================
    
    
    static let appThemeColor               = UIColor(r: 0, g: 162, b: 209,alpha: 1)

    static let appThemeFadeColor               = UIColor(r: 0, g: 162, b: 209,alpha: 0.5)

    static let continiueBtnColor = UIColor.white
    static let continiueBtnFadeColor = UIColor(r: 255, g: 255, b: 255,alpha: 0.5)


}
