//
//  AppUserDefaults+Key.swift
//  StarterProj
//
//  Created by MAC on 07/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation

extension AppUserDefaults {

    enum Key : String {
        
        case Accesstoken
        case displayName
        case email
        case firstName
        case lastName
        case countryCode
        case phone
        case status
        case userType
        case gender
        case dob
        case age
        case userId
        case userImage
        case otpVarified
        case is_email_verified
        case account_status

    }
}
