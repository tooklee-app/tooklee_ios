//
//  Globals.swift
//  TookLee
//
//  Created by Nanak on 13/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import Foundation
import UIKit


let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate

func print_debug <T> (_ object: T) {
    
    // TODO: Comment Next Statement To Deactivate Logs
    print(object)
    
}

var isSimulatorDevice:Bool {
    
    var isSimulator = false
    #if arch(i386) || arch(x86_64)
        //simulator
        isSimulator = true
    #endif
    return isSimulator
}

func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

//MARK:- ===========================
//MARK:- ShowToast

func showToastWithMessage(_ msg: String){
    sharedAppDelegate.window?.makeToast(msg)
}


//MARK:- Notyification Extension

extension Notification.Name{
    
   static let dismissViewNotification = Notification.Name(rawValue: "dismissViewNotification")
}
