//
//  CommonFunctions.swift
//  TookLee
//
//  Created by Nanak on 07/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import Foundation
import UIKit

struct CommonFunction {
    
    static func setButtonState(_ isEnable: Bool , sender: UIButton , color : UIColor){
        
        sender.isEnabled = isEnable
        
        if isEnable{
            sender.layer.borderColor = color.cgColor
            sender.setTitleColor(color, for: .normal)
        }else{
            sender.layer.borderColor = color.cgColor
            sender.setTitleColor(color, for: .normal)
        }
    }

}


