//
//  AppDelegate.swift
//  TookLee
//
//  Created by Nanak on 12/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import GoogleSignIn
import GoogleMaps
import IQKeyboardManager


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UITabBarDelegate,UITabBarControllerDelegate{

    var window: UIWindow?
    var parentNavigationController = UINavigationController()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GoogleLoginController.shared.configure(withClientId: GoogleAPIKeys.GoogleSignKey)
        
        GMSServices.provideAPIKey("AIzaSyDQ-hO9CZB57M1yf9AsDz9Umm5CLzuYPCg")
        
        //MARK: Facebook related
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = false

        if CurrentUser.isLoggedIn{
            
            NavigationManager.gotoHome()

        }else{
            NavigationManager.gotoLoginPage()

        }
        return true
        
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        //MARK: Facebook related
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url as URL!,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
    }

    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url as URL!,
            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplicationOpenURLOptionsKey.annotation] as Any
        )
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TookLee")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    
    //MARK: Tabbarviewcontroller delegates
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        
        let bgView1 = tabBarController.tabBar.viewWithTag(11211)
        let bgView2 = tabBarController.tabBar.viewWithTag(11212)
        let bgView3 = tabBarController.tabBar.viewWithTag(11213)
        let bgView4 = tabBarController.tabBar.viewWithTag(11214)
        let bgView5 = tabBarController.tabBar.viewWithTag(11215)

        switch tabBarController.selectedIndex {
            
        case 0:
            
            bgView1?.backgroundColor = AppColors.appThemeColor
            bgView2?.backgroundColor = UIColor.white
            bgView3?.backgroundColor = UIColor.white
            bgView4?.backgroundColor = UIColor.white
            bgView5?.backgroundColor = UIColor.white

        case 1:
            
            bgView1?.backgroundColor = UIColor.white
            bgView2?.backgroundColor = AppColors.appThemeColor
            bgView3?.backgroundColor = UIColor.white
            bgView4?.backgroundColor = UIColor.white
            bgView5?.backgroundColor = UIColor.white

        case 2:
            
            bgView1?.backgroundColor = UIColor.white
            bgView2?.backgroundColor = UIColor.white
            bgView3?.backgroundColor = AppColors.appThemeColor
            bgView4?.backgroundColor = UIColor.white
            bgView5?.backgroundColor = UIColor.white

        case 3:
            
            bgView1?.backgroundColor = UIColor.white
            bgView2?.backgroundColor = UIColor.white
            bgView3?.backgroundColor = UIColor.white
            bgView4?.backgroundColor = AppColors.appThemeColor
            bgView5?.backgroundColor = UIColor.white
        case 4:
            
            bgView1?.backgroundColor = UIColor.white
            bgView2?.backgroundColor = UIColor.white
            bgView3?.backgroundColor = UIColor.white
            bgView4?.backgroundColor = UIColor.white
            bgView5?.backgroundColor = AppColors.appThemeColor

        default:
            break
        }
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }

}

