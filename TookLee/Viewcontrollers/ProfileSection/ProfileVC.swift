//
//  ProfileVC.swift
//  TookLee
//
//  Created by Nanak on 10/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileVC: BaseVc {

    
    @IBOutlet weak var profileTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableView.delegate = self
        self.profileTableView.dataSource = self
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBAction func crossBtnTap(_ sender: UIButton) {
        
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
        
    }
    
    
}



//MARK:- UITabelview delegate and datasource
//MARK:- ===================================

extension ProfileVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (indexPath.row == 0) ? 200 : 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageCell", for: indexPath) as! ProfileImageCell
            
            cell.setUserInfo()
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileDetailCell", for: indexPath) as! UserProfileDetailCell
            cell.setUserData(with: indexPath)
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 1:
            NavigationManager.moveToEditProfile(with: .name)
        case 2:
            NavigationManager.moveToEditProfile(with: .email)

        case 3:
            NavigationManager.moveToEditProfile(with: .phone)

        case 4:
            NavigationManager.moveToEditProfile(with: .dob)

        default:
            print_debug("")
        }
    }
}

class ProfileImageCell: UITableViewCell {
    
    @IBOutlet weak var profieImage: UIImageView!
    @IBOutlet weak var ratingLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUserInfo(){
        
        guard let profile_pic = CurrentUser.profile_pic, !profile_pic.isEmpty else{
            return
        }
        
        self.profieImage.imageFromURl(profile_pic, placeHolderImage: UIImage(named: "ic_following_placeholder"), loader: false, completion: { (success) in
            
        })
        
    }
}


class UserProfileDetailCell: UITableViewCell {
    
    @IBOutlet weak var detailLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func setUserData(with indexPath : IndexPath){
        
        switch indexPath.row {
            
        case 1:
            
            guard let first_name = CurrentUser.first_name, let last_name = CurrentUser.last_name , !first_name.isEmpty , !last_name.isEmpty else{
                self.detailLbl.text = ""
                return
            }

            self.detailLbl.text = first_name + " " + last_name
            
        case 2:
            guard let email = CurrentUser.email,!email.isEmpty else{
                self.detailLbl.text = ""
                return
            }
            
            self.detailLbl.text = email

        case 3:
            guard let phone = CurrentUser.phone, let code = CurrentUser.country_code , !phone.isEmpty , !code.isEmpty else{
                self.detailLbl.text = ""
                return
            }
            
            self.detailLbl.text = code + phone

        case 4:
        
            guard let dob = CurrentUser.dob , !dob.isEmpty else{
                self.detailLbl.text = ""
                return
            }
            
            self.detailLbl.text = dob


        default:
            print_debug("")
        }
    }

}
