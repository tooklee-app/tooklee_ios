//
//  EditProfileVC.swift
//  TookLee
//
//  Created by Nanak on 16/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    
    var editState : EditProfileState = .name
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeBtnTap(_ sender: UIButton) {
        
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
    }
    
    @IBAction func editingChanged(_ sender: SkyFloatingLabelTextField) {
        
    }
    
    
    private func initialSetup(){
        
        switch self.editState {
            
        case .name:
            
            self.subTitleLbl.text = "Name"
            
        case .email:
            
            self.subTitleLbl.text = "Email"
            self.lastNameTextField.isHidden = true
            
        case .phone:
            
            self.subTitleLbl.text = "Phone Number"
            self.lastNameTextField.isHidden = true

        case .dob:
            self.subTitleLbl.text = "DOB"
            self.lastNameTextField.isHidden = true

        }
    }

}
