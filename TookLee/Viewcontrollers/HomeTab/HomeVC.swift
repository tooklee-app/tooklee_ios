//
//  HomeVC.swift
//  TookLee
//
//  Created by Nanak on 03/02/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var homeTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Methods
    //MARK:- ==========================================

    private func initialSetup(){
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
    }
}

// MARK:- TableView Delegate and datasource methods
//MARK:- ==========================================


extension HomeVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            
            return 150
        case 1:
            return 100
            
        case 2:
            return 180
            
        default:
            return 190
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! HeaderCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RemainingServicesCell", for: indexPath) as! RemainingServicesCell
            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectServiceCell", for: indexPath) as! SelectServiceCell
            return cell

        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClimateCell", for: indexPath) as! ClimateCell
            return cell

        }
    }
}

// MARK:- TableView Cell Classes
//MARK:- ==========================================

class HeaderCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

class RemainingServicesCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

class SelectServiceCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
class ClimateCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
