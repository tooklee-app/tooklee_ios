//
//  SideMenuVC.swift
//  TookLee
//
//  Created by Nanak on 10/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import SDWebImage

class SideMenuVC: UIViewController {

    @IBOutlet weak var sideMenuTableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.sideMenuTableView.delegate = self
        self.sideMenuTableView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


//MARK:- UITabelview delegate and datasource
//MARK:- ===================================

extension SideMenuVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (indexPath.row == 0) ? 200 : 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuProfileCell", for: indexPath) as! SideMenuProfileCell
            
            cell.setUserInfo()
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            guard CurrentUser.isLoggedIn else{
                
                showToastWithMessage("You are a guest user")
                return
                
            }

            closeLeft()
            NavigationManager.moveToProfile()

        case 1:
            NavigationManager.gotoLoginPage()
            AppUserDefaults.removeAllValues()

        default:
            print_debug("select")
        }
    }
}

class SideMenuProfileCell: UITableViewCell {
    
    @IBOutlet weak var profieImage: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var viewProfileBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUserInfo(){
        
        guard CurrentUser.isLoggedIn else{
            
            self.firstName.text = " Guest"
            self.lastName.text = "User"

            return
            
        }

        guard let first_name = CurrentUser.first_name, !first_name.isEmpty else{
            self.firstName.text = " Guest"
            return
        }
        
        self.firstName.text = first_name
        
        guard let last_name = CurrentUser.last_name, !last_name.isEmpty else{
            self.lastName.text = ""

            return
        }

        self.lastName.text = last_name
        
        guard let profile_pic = CurrentUser.profile_pic, !profile_pic.isEmpty else{
            return
        }

        self.profieImage.imageFromURl(profile_pic, placeHolderImage: UIImage(named: "ic_following_placeholder"), loader: false, completion: { (success) in
            
        })

    }
}


class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var menuLbl: UILabel!
    @IBOutlet weak var separaotorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
