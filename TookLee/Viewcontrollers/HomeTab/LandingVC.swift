//
//  LandingVC.swift
//  TookLee
//
//  Created by Nanak on 05/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import GoogleMaps

class LandingVC: UIViewController , LocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var sideMenuBtn: UIButton!
    @IBOutlet weak var dragableView: UIView!
    @IBOutlet weak var dragHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var dragTableView: UITableView!
    
    var locationManager = LocationManager.sharedInstance
    var tapGasture = UITapGestureRecognizer()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        let rideNib = UINib(nibName: "HomeOfferRideCell", bundle: nil)
        self.dragTableView.register(rideNib, forCellReuseIdentifier: "HomeOfferRideCell")
        
        let climateNib = UINib(nibName: "HomeClimateCell", bundle: nil)
        self.dragTableView.register(climateNib, forCellReuseIdentifier: "HomeClimateCell")
        
        let climateImageNib = UINib(nibName: "ClimateImageCell", bundle: nil)
        self.dragTableView.register(climateImageNib, forCellReuseIdentifier: "ClimateImageCell")


        self.addSwipeGesture(toView: self.view)
        self.dragTableView.delegate = self
        self.dragTableView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func sideMenuBtnTap(_ sender: UIButton) {
        
        openLeft()
        
    }
    
    func locationFound(_ latitude:Double, longitude:Double) {
        
        self.mapView.camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14.50)
        mapView.isMyLocationEnabled = true
//        mapView.setMinZoom(4.6, maxZoom: 8)

    }

    
    func addSwipeGesture(toView view: UIView) {
        
        let downSwipeGestureRecogniser = UISwipeGestureRecognizer(target: self, action: #selector(viewDidSwipe(_:)))
        let upSwipeGestureRecogniser = UISwipeGestureRecognizer(target: self, action: #selector(viewDidSwipe(_:)))
        
        downSwipeGestureRecogniser.direction = .down
        upSwipeGestureRecogniser.direction = .up
        view.addGestureRecognizer(downSwipeGestureRecogniser)
        view.addGestureRecognizer(upSwipeGestureRecogniser)
    }
    
    
    @objc func viewDidSwipe(_ swipe: UISwipeGestureRecognizer) {
        
        
        if swipe.direction == .up {
            
            self.openRideView()
            
        } else if swipe.direction == .down {
            
            self.closeRideView()
        }
    }
    
    func openRideView() {
        
        self.tapGasture = UITapGestureRecognizer(target: self, action: #selector(self.closeRideView))
        self.view.addGestureRecognizer(self.tapGasture)
        UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions(), animations: {
            
            self.dragHeightConstant.constant = UIScreen.main.bounds.height - 40
            self.sideMenuBtn.isHidden = true

            self.view.layoutIfNeeded()
            
        }, completion: { (didComplete: Bool) in
            
        })
    }
    
    
    @objc func closeRideView() {
        self.mapView.removeGestureRecognizer(self.tapGasture)
        UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.dragHeightConstant.constant = 90
            self.sideMenuBtn.isHidden = false
            self.view.layoutIfNeeded()
            
        }, completion: { (didComplete: Bool) in
            
        })
    }
    
    @objc private func getStartedTap( _ sender : UIButton){
        
        print_debug("Get Started")
    }
    
}


extension LandingVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 || indexPath.row == 2{
            
            return ((UIScreen.main.bounds.height - 40) / 2) - 40

        }else{
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeOfferRideCell", for: indexPath) as! HomeOfferRideCell
            cell.getStartedBtn.addTarget(self, action: #selector(self.getStartedTap(_:)), for: .touchUpInside)
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClimateImageCell", for: indexPath) as! ClimateImageCell
            return cell

        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeClimateCell", for: indexPath) as! HomeClimateCell
            cell.getStartedBtn.addTarget(self, action: #selector(self.getStartedTap(_:)), for: .touchUpInside)

            return cell

        default:
            fatalError()
        }
    }
}

