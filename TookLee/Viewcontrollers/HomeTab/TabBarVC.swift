//
//  TabBarVC.swift
//  TookLee
//
//  Created by Nanak on 05/02/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarItem.image = nil
        self.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -15)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Avenir-Medium", size:15)!, NSAttributedStringKey.foregroundColor: UIColor.black], for: .normal)
        self.initialSetup()
    }
    
    func initialSetup(){
        
        let safeArea = self.tabBar.safeAreaInsets.bottom
        
        UITabBar.appearance().tintColor = UIColor.white
        self.view.backgroundColor = UIColor.black
        var frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width/5, height: 55)
        let bgView1 = UIView(frame: frame)
        bgView1.tag = 11211
        bgView1.backgroundColor = AppColors.appThemeColor
        self.tabBar.addSubview(bgView1)
        self.tabBar.sendSubview(toBack: bgView1)
        
        frame = CGRect(x: self.view.bounds.size.width/5, y: 0.0, width: self.view.bounds.size.width/5, height: 55)
        let bgView2 = UIView(frame: frame)
        bgView2.tag = 11212
        bgView2.backgroundColor = UIColor.white
        self.tabBar.addSubview(bgView2)
        self.tabBar.sendSubview(toBack: bgView2)
        
        frame = CGRect(x: 2*self.view.bounds.size.width/5, y: 0.0, width: self.view.bounds.size.width/5, height: 55)
        let bgView3 = UIView(frame: frame)
        bgView3.tag = 11213
        bgView3.backgroundColor = UIColor.white
        self.tabBar.addSubview(bgView3)
        self.tabBar.sendSubview(toBack: bgView3)
        
        frame = CGRect(x: 3*self.view.bounds.size.width/5, y: 0.0, width: self.view.bounds.size.width/5, height: 55)
        let bgView4 = UIView(frame: frame)
        bgView4.tag = 11214
        bgView4.backgroundColor = UIColor.white
        self.tabBar.addSubview(bgView4)
        self.tabBar.sendSubview(toBack: bgView4)

        frame = CGRect(x: 4*self.view.bounds.size.width/5, y: 0.0, width: self.view.bounds.size.width/5, height: 55)
        let bgView5 = UIView(frame: frame)
        bgView5.tag = 11215
        bgView5.backgroundColor = UIColor.white
        self.tabBar.addSubview(bgView5)
        self.tabBar.sendSubview(toBack: bgView5)

        self.tabBar.itemPositioning = UITabBarItemPositioning.fill

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
