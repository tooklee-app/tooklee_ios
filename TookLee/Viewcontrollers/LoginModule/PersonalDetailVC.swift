//
//  PersonalDetailVC.swift
//  TookLee
//
//  Created by Nanak on 25/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import Photos
import AVFoundation


class PersonalDetailVC: BaseVc {

    enum CreateAccountType {
        case email
        case facebook
        case google
    }
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var detailTablevVew: UITableView!
    
    fileprivate enum ToolBarState {
        case cancel,done,none
    }

    var personalDetail = JSONDictionary()
    var detailType : PersonalDetailType = .email
    var profileImage:UIImage?
    let toolBar = UIToolbar()
    let datePickerView = UIDatePicker()
    let imagePicker = UIImagePickerController()
    var accountType : CreateAccountType = .email
    
    fileprivate var selectedToolBarOption : ToolBarState = ToolBarState.none


    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: OperationQueue.main) { (notification:Notification!) -> Void in
            
            self.view.addGestureRecognizer(tapGesture)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: OperationQueue.main) { _ in
            
            self.view.removeGestureRecognizer(tapGesture)
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }

    
    //MARK:- ================
    //MARK:- Private methods
    
    @objc func dismissKeyboard(_sender: AnyObject){
        
        self.view.endEditing(true)
    }

    
    private func initialSetup(){
        
        self.view.applyGradient()

        self.detailTablevVew.delegate = self
        self.detailTablevVew.dataSource = self
        self.imagePicker.delegate = self
        self.toolBarSetting()
        self.datePickerSetting()

    }
    
    fileprivate func datePickerSetting(){
        
        datePickerView.backgroundColor = UIColor.white
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePickerView.addTarget(self, action: #selector(self.handleDatePicker(_:)), for: UIControlEvents.valueChanged)
    }
    
    @objc fileprivate func doneToolBarButtonTapped(){
        
        selectedToolBarOption = .done
        self.view.endEditing(true)
    }
    
    
    @objc fileprivate func cancelToolBarButtonTapped(){
        selectedToolBarOption = .cancel
        self.view.endEditing(true)
    }
    
    @objc fileprivate func handleDatePicker(_ sender: UIDatePicker) {
        
    }
    
    fileprivate func toolBarSetting(){
        
        toolBar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: StringConstants.Cancel.localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelToolBarButtonTapped))
        let doneButton = UIBarButtonItem(title: StringConstants.Done.localized, style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneToolBarButtonTapped))
        
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        toolBar.setItems([cancelButton,space,doneButton], animated: true)
    }
    
    private func isValidateDetail() -> Bool{
        
        guard let email = self.personalDetail["email"] as? String, !email.isEmpty else{
            showToastWithMessage(StringConstants.Enter_Email.localized)
            return false
        }
        if email.checkIfInvalid(.email){
            showToastWithMessage(StringConstants.Invalid_Email.localized)
        }
        guard let fisrt_name = self.personalDetail["first_name"] as? String, !fisrt_name.isEmpty else{
            showToastWithMessage(StringConstants.Enter_First_Name.localized)

            return false
        }
        guard let last_name = self.personalDetail["last_name"] as? String, !last_name.isEmpty else{
            showToastWithMessage(StringConstants.Enter_Last_Name.localized)

            return false
        }
        return true
    }
    
    private func hitCreateProfileService(){
        
        var image = [String : UIImage]()
        if self.profileImage != nil{
            image["profile_pic"] = self.profileImage!
        }
        WebServices.createProfileapi(parameters: self.personalDetail, image: image, webServiceSuccess: { (success, msg, data) in
            
            if success{
                _ = User(json: data)
                
                let token = CurrentUser.accessToken ?? ""
                
                if self.accountType == .facebook || self.accountType == .google{
                    
                    NavigationManager.movetoSelectPaymentType()
                    
                }else{
                    NavigationManager.moveToVarifyEmailOtp(otpState: .varifyEmail, token: token, email: self.personalDetail["email"] as! String)

                }
            }else{
                
                showToastWithMessage(msg)
            }
        }) { (e) -> (Void) in
            print_debug(e.localizedDescription)
        }
    }
    
    private func makefacebookSignupRequiredParams(with fbData : FacebookModel) -> JSONDictionary{
        
        var details = JSONDictionary()
        details["first_name"] = fbData.first_name
        details["last_name"] = fbData.last_name
        details["email"] = fbData.email
        details["Profile_pic"] = fbData.picture?.absoluteString
        
        if let gender = fbData.gender {
            
            details["gender"] = gender.rawValue
        }
        
        details["age"] = ""
        details["dob"] = ""
        details["facebook_id"] = fbData.id
        
        //dob?.toString(dateFormat: "MM/dd/yyyy")
        // details["password"] = password
        return details
        
    }
    
    //MARK:- ================
    //MARK:- IBActions

    @IBAction func backBtnTap(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textFieldEditing(_ sender: SkyFloatingLabelTextField) {

    }
    
    @objc func emailBtnTap(_ sender: UIButton) {
        self.accountType = .email
        self.personalDetail = [:]
        self.profileImage = nil
        self.personalDetail["is_email_verified"] = "0"
        self.detailTablevVew.reloadData()

    }
    
    @objc func facebookBtnTap(_ sender: UIButton) {
        self.accountType = .facebook

        self.personalDetail = [:]
        self.profileImage = nil

        FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result) in
            
            self.personalDetail = self.makefacebookSignupRequiredParams(with: result)
            self.personalDetail["is_email_verified"] = "1"
            self.detailTablevVew.reloadData()

        }) { (error) in
            print(error?.localizedDescription ?? "")
        }

    }

    @objc func googleBtnTap(_ sender: UIButton) {
        self.accountType = .google
        
        self.personalDetail = [:]
        self.profileImage = nil
        GoogleLoginController.shared.login(success: { (model :  GoogleUser) in
            
            self.personalDetail = model.dictionaryObject
            self.personalDetail["is_email_verified"] = "1"

            self.detailTablevVew.reloadData()
            
        })
        { (err : Error) in
            print(err.localizedDescription)
        }

    }

    @IBAction func continueTap(_ sender: UIButton) {
        
        if self.isValidateDetail(){
            
            self.personalDetail["device_token"] = DeviceDetail.deviceToken
            personalDetail["device_id"] = DeviceDetail.device_id
            personalDetail["device_model"] = DeviceDetail.device_model
            personalDetail["os_version"] = DeviceDetail.os_version
            personalDetail["platform"] = "2"

            print_debug(self.personalDetail)
            self.hitCreateProfileService()
        }
//        let obj = CardDetailVC.instantiate(fromAppStoryboard: .Main)
//        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @objc private func textFieldTextChanged(_ textField : SkyFloatingLabelTextField){
        print_debug(textField.text)
        
        if textField.tag == 2{
            self.personalDetail["email"] = textField.text!
        }else if textField.tag == 3{
            self.personalDetail["first_name"] = textField.text!
        }else if textField.tag == 4{
            self.personalDetail["last_name"] = textField.text!
        }
    }
}

extension PersonalDetailVC: UITextFieldDelegate{

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    guard let indexPath = textField.tableViewIndexPath(self.detailTablevVew) else {
        return false
    }
    if indexPath.row == 5{
        textField.tintColor = UIColor.clear
        textField.inputView = datePickerView
        textField.inputAccessoryView = toolBar
    }
    return true
    
}
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.detailTablevVew) else {
            return
        }
        if indexPath.row == 5{
            if selectedToolBarOption == ToolBarState.done {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                //let date = dateFormatter.string(from: datePickerView.date)
                let dob = datePickerView.date
                self.personalDetail["dob"] = dateFormatter.string(from: dob)
                self.detailTablevVew.reloadRows(at: [IndexPath(row : 4, section : 0)], with: .none)
            }
        }
    }
}
extension PersonalDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return 100

        case 0:
            return 115
        case 1,2,3,4,5:
            return 70
        default:
            return 80
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalDetailTypeButtonCell", for: indexPath) as! PersonalDetailTypeButtonCell
            cell.emailBtn.addTarget(self, action: #selector(self.emailBtnTap(_:)), for: .touchUpInside)
            cell.facebookBtn.addTarget(self, action: #selector(self.facebookBtnTap(_:)), for: .touchUpInside)
            cell.googleBtn.addTarget(self, action: #selector(self.googleBtnTap(_:)), for: .touchUpInside)

            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePicCell", for: indexPath) as! ProfilePicCell
            cell.picSelectBtn.addTarget(self, action: #selector(self.onTapEditImage), for: .touchUpInside)
            cell.popuLateData(data: self.personalDetail, profileImage: self.profileImage)
            return cell
            
        case 2,3,4,5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalDetailCell", for: indexPath) as! PersonalDetailCell
            cell.detailTextField.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
            cell.detailTextField.tag = indexPath.row
            cell.detailTextField.delegate = self
            cell.populateData(with: self.personalDetail, indexPath: indexPath)
            return cell

        default:
            fatalError()
        }
        return UITableViewCell()
    }
}


//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension PersonalDetailVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @objc func onTapEditImage(){
        
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    //Pic image from Gallery
    func openCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            switch authStatus {
            case .authorized:
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            case .denied, .restricted:
                self.alertToEncourageCameraAccess("Camera")
            case .notDetermined:
                self.requestForCameraAccessPermission()
            }
        }else{
            
            let alertViewController = UIAlertController(title: "Camera Unavailable", message: "Please check to see if it is disconnected or in use by another application", preferredStyle: UIAlertControllerStyle.alert)
            _ = UIAlertAction(title: StringConstants.Ok.localized, style: UIAlertActionStyle.default) { (action : UIAlertAction) -> Void in
                
                alertViewController.dismiss(animated: true, completion: nil)
                
            }
        }
    }
    
    func alertToEncourageCameraAccess(_ service: String) {
        
        var alertController = UIAlertController()
        
        if service == "Camera"{
            
            alertController = UIAlertController (title: "", message: "You don't have permission to access the camera. Please go to device settings and enable the camera permissions.", preferredStyle: .alert)
            
        }else{
            
            alertController = UIAlertController (title: "", message: "You don't have permission to access the gallery. Please go to device settings and enable the gallery permissions.", preferredStyle: .alert)
            
        }
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil);
        
    }
    
    func requestForCameraAccessPermission() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
            if granted {
                self.openCamera()
            } else {
                self.alertToEncourageCameraAccess("Camera")
            }
        }
    }
    
    func openGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            
            self.galleryPermission()
            
        } else{
            let alertViewController = UIAlertController(title: "Gallery Unavailable", message: "Please check to see if it is in use by another application", preferredStyle: UIAlertControllerStyle.alert)
            _ = UIAlertAction(title: StringConstants.Ok.localized, style: UIAlertActionStyle.default) { (action : UIAlertAction) -> Void in
                
                alertViewController.dismiss(animated: true, completion: nil)
                
            }
        }
    }
    
    //    MARK:- For Gallery Permission
    func galleryPermission() {
        
        let galleryAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch galleryAuthorizationStatus{
            
        case PHAuthorizationStatus.authorized:
            self.present(self.imagePicker, animated: true, completion: nil)
            
        case .restricted, .denied:
            self.alertToEncourageCameraAccess("Gallery")
            
        case.notDetermined:
            PHPhotoLibrary.requestAuthorization() { status in
                self.galleryPermission()
            }
        }
    }
    
    
    //imagePickerControllerdelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            self.profileImage = pickedImage
            
            self.detailTablevVew.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
        dismiss(animated: true, completion: nil)
    }
}

class ProfilePicCell: UITableViewCell {
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var cameraImg: UIImageView!
    @IBOutlet weak var picSelectBtn: UIButton!
    
    func popuLateData( data : JSONDictionary , profileImage : UIImage?){
        
        if profileImage != nil{
            self.profilePic.image = profileImage
        }
        else if let imageName = data["Profile_pic"] as? String , !imageName.isEmpty{
            
            self.profilePic.imageFromURl(imageName, placeHolderImage: UIImage(named: "ic_following_placeholder"), loader: false, completion: { (success) in
                
            })
        }else{
            self.profilePic.image = UIImage(named: "ic_following_placeholder")
        }

    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class PersonalDetailCell: UITableViewCell {
    
    @IBOutlet weak var detailTextField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.detailTextField.text = ""
    }
    
    func populateData(with data : JSONDictionary , indexPath : IndexPath){
        
        switch indexPath.row {
        case 2:
            self.detailTextField.placeholder = "Email Address"

            if let email = data["email"] as? String, !email.isEmpty{
                self.detailTextField.text = email
                self.detailTextField.title = "Email Address"

            }
        case 3:
            self.detailTextField.placeholder = "First Name"
            if let first_name = data["first_name"] as? String, !first_name.isEmpty{
                self.detailTextField.text = first_name
                self.detailTextField.title = "First Name"

            }

        case 4:
            self.detailTextField.placeholder = "Last Name"
            if let last_name = data["last_name"] as? String, !last_name.isEmpty{
                self.detailTextField.text = last_name
                self.detailTextField.title = "Last Name"

            }

        case 5:

            self.detailTextField.placeholder = "Date of Birth"
            if let dob = data["dob"] as? String, !dob.isEmpty{
                self.detailTextField.text = dob
                self.detailTextField.title = "Date of Birth"

            }

        default:
            fatalError()
        }
        
    }

}


class PersonalDetailTypeButtonCell: UITableViewCell {
    
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
