//
//  LoginSignupOptionVC.swift
//  TookLee
//
//  Created by Nanak on 03/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import Foundation
import TTTAttributedLabel

class LoginSignupOptionVC: BaseVc {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var createUserBTn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var createAccountBtn: UIButton!
    @IBOutlet weak var privacyPolicyLbl: TTTAttributedLabel!
    
    let privacyPolicyStr = "By tapping sign in or create account, I agree Toklees's Terms's of Services, Payment Terms and Privacy policy"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBActions
    //MARK:- ==============================

    @IBAction func guestUserTap(_ sender: UIButton) {
        NavigationManager.gotoHome()
    }
    
    @IBAction func signInTap(_ sender: UIButton) {
        
        NavigationManager.moveToLoginWithPhone(.signIn)
    }

    @IBAction func createUserTap(_ sender: UIButton) {
        NavigationManager.moveToLoginWithPhone(.signUp)

    }

    
    private func initialSetup(){
        
        self.view.applyGradient()
//        self.bgView.layer.masksToBounds = false
//        self.view.addGradient(topColor: UIColor.yellow, bottomColor: AppColors.appThemeColor)
        self.privacyPolicyLbl.delegate = self
        self.privacyPolicyLbl.text = privacyPolicyStr
        let str:NSString = self.privacyPolicyLbl.text! as NSString
        let range : NSRange = str.range(of: "Terms's of Services")
        let range1 : NSRange = str.range(of: "Payment Terms")
        let range2 : NSRange = str.range(of: "Privacy policy")

        
        self.privacyPolicyLbl.addLink(to: NSURL(string: "terms")! as URL!, with: range)
        self.privacyPolicyLbl.addLink(to: NSURL(string: "payment")! as URL!, with: range1)
        self.privacyPolicyLbl.addLink(to: NSURL(string: "privacy")! as URL!, with: range2)


    }
}


//MARK:- TTTAtributedLbel Delegate method
//MARK:- ==============================

extension LoginSignupOptionVC : TTTAttributedLabelDelegate{
    // attriuted label didselect method
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        let obj = TermsServicesVC.instantiate(fromAppStoryboard: .Main)

        if url.absoluteString == "terms"{
            
            obj.vcState = .Terms
        }else if url.absoluteString == "payment"{
            obj.vcState = .Payment

        }else{
            obj.vcState = .Privacy

        }
        sharedAppDelegate.parentNavigationController.pushViewController(obj, animated: true)

    }
    
}
