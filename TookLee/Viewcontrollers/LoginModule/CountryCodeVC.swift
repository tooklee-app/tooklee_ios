//
//  CountryCodeVC.swift
//  Mutadawel
//
//  Created by Appinventiv on 29/03/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SetContryCodeDelegate {
    func setCountryCode(country_info: JSONDictionary)
}

class CountryCodeVC: UIViewController {

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backBtn: UIButton!
//    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var countryCodeTableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UITextField!
    
    
    var countryInfo = JSONDictionaryArray()
    var filteredCountryList = JSONDictionaryArray()
    var delegate:SetContryCodeDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    private func initialSetup(){
    
        self.countryCodeTableView.delegate = self
        self.countryCodeTableView.dataSource = self
        self.searchBar.delegate = self
        self.searchView.layer.cornerRadius = self.searchView.bounds.height / 2
//        self.navigationTitle.text = "Country Code"
        self.searchView.backgroundColor = UIColor.clear
        self.searchView.layer.borderWidth = 1
        self.searchView.layer.borderColor = UIColor.white.cgColor
        self.searchBar.setPlaceHolder(with: "Search", with: AppColors.continiueBtnFadeColor)
        self.searchBar.textColor = AppColors.continiueBtnColor
        //rotateBackImage(backBtn: self.backBtn)
        self.readJson()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func readJson() {
        
        let file = Bundle.main.path(forResource: "countryData", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        print_debug(data)
        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as! JSONDictionaryArray
        
        self.countryInfo = jsonData!
        self.filteredCountryList = jsonData!
        self.countryCodeTableView.reloadData()
        print_debug(jsonData)
        
    }

    
    @IBAction func backbtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    
}

extension CountryCodeVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        

        delayWithSeconds(0.01) {
            print_debug(textField.text!)

            let filter = self.countryInfo.filter({ ($0["CountryEnglishName"] as? String ?? "").localizedCaseInsensitiveContains(textField.text!)
        })
            print_debug(filter)
            self.filteredCountryList = filter
            
            if textField.text == ""{
                self.filteredCountryList = self.countryInfo
            }
            self.countryCodeTableView.reloadData()

        }
        return true
    }
    
}

extension CountryCodeVC: UITableViewDelegate,UITableViewDataSource{


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCountryList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeCell", for: indexPath) as! CountryCodeCell
        let info = self.filteredCountryList[indexPath.row]
        cell.populateView(info: info)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info = self.filteredCountryList[indexPath.row]

        self.delegate.setCountryCode(country_info: info)
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}


class CountryCodeCell: UITableViewCell {
    
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.countryFlag.layer.cornerRadius = self.countryFlag.bounds.height / 2
        self.countryFlag.layer.masksToBounds = true
        self.countryFlag.contentMode = .scaleAspectFit
        self.countryFlag.backgroundColor = #colorLiteral(red: 0.3843137255, green: 0.8274509804, blue: 0.9803921569, alpha: 1)
    }
    
    
    func populateView(info: JSONDictionary){
        
        guard let code = info["CountryCode"] else{return}
        guard let name = info["CountryEnglishName"] as? String else{return}
        self.countryCode.text = "+\(code)  -  \(name)"
        guard let img = info["CountryFlag"] as? String, !img.isEmpty else{
            self.countryFlag.image = UIImage(named: "country_place_holder")
            return
            
        }
            self.countryFlag.image = UIImage(named: img)

    }
}
