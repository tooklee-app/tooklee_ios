//
//  ChangeEmailVC.swift
//  TookLee
//
//  Created by Nanak on 05/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class ChangeEmailVC: BaseVc {

    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var continiueBtn: UIButton!
    
    var emailState : EnterEmailState = .varifyEmail
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.continiueBtnEnableDisable(false)
        self.emailTextField.setPlaceHolder(with: "Email Address", with: AppColors.continiueBtnFadeColor)
        self.emailTextField.delegate = self
        self.view.applyGradient()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func continiueBtnEnableDisable(_ isEnable: Bool){
        
        self.continiueBtn.isEnabled = isEnable
        
        if isEnable{
            self.continiueBtn.layer.borderColor = AppColors.continiueBtnColor.cgColor
            self.continiueBtn.setTitleColor(AppColors.continiueBtnColor, for: .normal)
        }else{
            self.continiueBtn.layer.borderColor = AppColors.continiueBtnFadeColor.cgColor
            self.continiueBtn.setTitleColor(AppColors.continiueBtnFadeColor, for: .normal)
        }
    }
    
    private func sendOtpOnEmail(){
        
        let email = self.emailTextField.text!

        var params = JSONDictionary()
        
        params["email"] = email
        
        WebServices.sendOTPOnEmailAPI(parameters: params, webServiceSuccess: { (success, msg, json) in
            
            if success{
                
                let token = json["token"].stringValue
                AppUserDefaults.save(value: token, forKey: .Accesstoken)
                NavigationManager.moveToVarifyEmailOtp(otpState: self.emailState, token: token, email: email)

            }else{
                
                showToastWithMessage(msg)
            }

        }) { (err) -> (Void) in
            
        }
    }
    
    
    @IBAction func backBtnTapp(_ sender: UIButton) {
        
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
    }
    
    @IBAction func editingChange(_ sender: SkyFloatingLabelTextField) {
        
        guard self.emailTextField.text!.checkIfValid(.email) else {
            self.continiueBtnEnableDisable(false)
            return
        }
        self.continiueBtnEnableDisable(true)
        
    }
    
    @IBAction func continiueBtnTap(_ sender: UIButton) {
        self.sendOtpOnEmail()
    }
    
}

//MARK:- UITextfield delegate
//MARK:- ===================================


extension ChangeEmailVC : UITextFieldDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && string == " "{
            return false
        }
        
        return true
        
        
    }
    
    

}
