//
//  WelcomeVC.swift
//  TookLee
//
//  Created by Nanak on 05/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class WelcomeVC: BaseVc {

    @IBOutlet weak var congratesLbl: UILabel!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var continiueBtn: UIButton!
    @IBOutlet weak var congratesHeight: NSLayoutConstraint!
    
    var isLogin = true
    var msgStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func continiueBtnTap(_ sender: UIButton) {
        
        NavigationManager.gotoHome()
    }
    
    
    private func initialSetup(){
        
        self.view.applyGradient()

        if self.isLogin{
            
            self.congratesHeight.constant = 0
            let name = CurrentUser.first_name!
            let welcome = "Hi \(name), Welcome Back"
            self.welcomeLbl.text = welcome

        }else{
            self.congratesHeight.constant = 30
            self.congratesLbl.text = StringConstants.Congratulations.localized.capitalized
            self.welcomeLbl.text = StringConstants.ChangeNOMsg.localized
        }
    }

}
