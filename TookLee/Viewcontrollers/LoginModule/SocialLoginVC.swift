//
//  SocialLoginVC.swift
//  TookLee
//
//  Created by Nanak on 25/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SocialLoginVC: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var orLbl: UILabel!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }

    @IBAction func emailEditing(_ sender: SkyFloatingLabelTextField) {
        
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func faceBookTap(_ sender: UIButton) {
        FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result) in
            
            let detal = self.makefacebookSignupRequiredParams(with: result)
            NavigationManager.gotoPersonalDetail(from: self, detailType: .facebook, detail: detal)
            
        }) { (error) in
            print(error?.localizedDescription ?? "")
        }
    }
    
    @IBAction func googleTap(_ sender: UIButton) {
        
        GoogleLoginController.shared.login(success: { (model :  GoogleUser) in
            
            NavigationManager.gotoPersonalDetail(from: self, detailType: .gmail, detail: model.dictionaryObject)

        })
        { (err : Error) in
            print(err.localizedDescription)
        }

    }
    
    @IBAction func continueTap(_ sender: UIButton) {
        
        NavigationManager.gotoPersonalDetail(from: self)
        
    }
    
    
    private func makefacebookSignupRequiredParams(with fbData : FacebookModel) -> JSONDictionary{
        
        var details = JSONDictionary()
        details["first_name"] = fbData.first_name
        details["last_name"] = fbData.last_name
        details["email"] = fbData.email
        details["Profile_pic"] = fbData.picture?.absoluteString

        if let gender = fbData.gender {
            
            details["gender"] = gender.rawValue
        }
        
        details["age"] = ""
        details["dob"] = ""
        details["facebook_id"] = fbData.id
        
        //dob?.toString(dateFormat: "MM/dd/yyyy")
        // details["password"] = password
        
        details["device_token"] = DeviceDetail.deviceToken
        details["device_id"] = DeviceDetail.device_id
        details["device_model"] = DeviceDetail.device_model
        details["os_version"] = DeviceDetail.os_version
        details["platform"] = "2"
        
        return details
        
    }
}
