//
//  SelectPaymentType.swift
//  TookLee
//
//  Created by Nanak on 04/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class SelectPaymentTypeVC: BaseVc {

    enum SelectedPayment {
        case card
        case cash
        case none
    }
    
    var selectedType = SelectedPayment.none
    var selectedIndexPath = IndexPath(row: 0, section: 10)
    var cardDetail = JSONDictionary()
    
    //MARK:- IBOutlets
    //MARK:- ===================================

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var paymentTypeTableView: UITableView!
    
    //MARK:- View life cycle
    //MARK:- ===================================

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initialSetup()
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }

    //MARK:- IBActions
    //MARK:- ===================================

    @IBAction func backBtnTap(_ sender: UIButton) {
        
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
        
    }
    
    @IBAction func continiueBtnTap(_ sender: UIButton) {
        
        NavigationManager.gotoHome()
    }
    
    @IBAction func selectCountryTap(_ sender: UIButton) {
        let obj = CountryCodeVC.instantiate(fromAppStoryboard: .Main)
        obj.delegate = self
        self.navigationController?.pushViewController(obj, animated: true)

    }
    
}



//MARK:- Private Methods
//MARK:- ===================================

extension SelectPaymentTypeVC{
    
    private func initialSetup(){

        cardDetail["countryFlag"] = "poland.png"
        cardDetail["country_name"] = "Poland"
        self.paymentTypeTableView.delegate = self
        self.paymentTypeTableView.dataSource = self
        self.backBtn.isHidden = true
    }
    
    @objc func selectBtnTap(_ sender : UIButton){
        
        guard let indexPath = sender.tableViewIndexPath(self.paymentTypeTableView) else {return}
        if indexPath.section == 0{
            self.selectedType = .card
        }else if indexPath.section == 1{
            self.selectedType = .cash
        }else if indexPath.section == 2{
            self.selectedType = .none
        }
        self.paymentTypeTableView.reloadData()
    }
    
    

}

//MARK:- Setcountry code delegate
//MARK:- ===================================

extension SelectPaymentTypeVC: SetContryCodeDelegate{
    
    func setCountryCode(country_info: JSONDictionary) {
        
        guard let country_name = country_info["CountryEnglishName"] as? String else{return}
        self.cardDetail["country_name"] = country_name
        guard let countryFlag = country_info["CountryFlag"] as? String else{return self.paymentTypeTableView.reloadData()}
        self.cardDetail["countryFlag"] = countryFlag
        self.paymentTypeTableView.reloadData()
    }
}


//MARK:- Tableview delegate and datasounrce
//MARK:- ===================================


extension SelectPaymentTypeVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            
            return 2
            
        }else{
            return 1

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            
            if indexPath.row == 0{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTyepCell", for: indexPath) as! PaymentTyepCell
                cell.selectBtn.addTarget(self, action: #selector(self.selectBtnTap(_:)), for: .touchUpInside)
                cell.setupViews(with: indexPath, selectedIndexPath: self.selectedIndexPath)
                return cell

            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CardDetailCell", for: indexPath) as! CardDetailCell
                cell.populateData(with: self.cardDetail)
                return cell

            }
            
        case 1,2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTyepCell", for: indexPath) as! PaymentTyepCell
            cell.setupViews(with: indexPath, selectedIndexPath: self.selectedIndexPath)
            cell.selectBtn.addTarget(self, action: #selector(self.selectBtnTap(_:)), for: .touchUpInside)
            
            return cell

            
        default:
            fatalError("Extra cell returned")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row == 0 else {
            return
        }
        self.selectedIndexPath = indexPath
        self.paymentTypeTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
            
        case 0:
            
            if indexPath.section == self.selectedIndexPath.section{
                
                if indexPath.row == 0{
                    return 80

                }else{
                    return 250

                }
                
            }else{
                if indexPath.row == 0{
                    return 80
                    
                }else{
                    return 0
                    
                }

            }
            
        case 3:
            return 100

        default:
            return 80

        }
    }
}

class CardDetailCell: UITableViewCell{
    
    @IBOutlet weak var cardNoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cvvTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var expiryDateTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryNameValueLbl: UILabel!
    @IBOutlet weak var countrySelectBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 60))
//        rightView.backgroundColor = UIColor.white
//        cardNoTextField.leftView = rightView
//        cardNoTextField.leftViewMode = .always
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func populateData(with cardDetail : JSONDictionary){
        
        if let cardNo = cardDetail["cardNo"] as? String, !cardNo.isEmpty{
            self.cardNoTextField.text = cardNo
        }else{
            self.cardNoTextField.text = ""
        }
        
        if let expiryDate = cardDetail["expiryDate"] as? String, !expiryDate.isEmpty{
            self.expiryDateTextField.text = expiryDate

        }else{
            self.expiryDateTextField.text = ""

        }
        
        if let cvv = cardDetail["cvv"] as? String, !cvv.isEmpty{
            self.cvvTextField.text = cvv

        }else{
            self.cvvTextField.text = ""

        }
        
        if let country_name = cardDetail["country_name"] as? String, !country_name.isEmpty{
            countryNameValueLbl.text = country_name
        }else{
            countryNameValueLbl.text = ""
        }
        
        if let countryFlag = cardDetail["countryFlag"] as? String, !countryFlag.isEmpty{
            self.countryFlag.image = UIImage(named: countryFlag)
        }else{
            self.countryFlag.image = UIImage(named: "poland.png")
        }
    }
}

class PaymentTyepCell: UITableViewCell{
    
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setupViews(with indexPath : IndexPath , selectedIndexPath : IndexPath){
        
        if indexPath == selectedIndexPath{
            self.selectBtn.backgroundColor = UIColor.white
        }else{
            self.selectBtn.backgroundColor = UIColor.clear
        }
        
    }
    

}


class ContiniueBtnCell: UITableViewCell{
    
    @IBOutlet weak var continiueBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

