//
//  SignupSuccessFullVC.swift
//  TookLee
//
//  Created by Nanak on 26/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class TermsServicesVC: BaseVc {

    enum VcState {
        case Terms
        case Payment
        case Privacy

    }
    
    var vcState: VcState = .Terms
    
    @IBOutlet weak var navigationLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.vcState == .Terms{
            
            self.navigationLbl.text = "Term's of Services"
            
        }else if self.vcState == .Payment{
            
            self.navigationLbl.text = "Payment Terms"

        }else{
            
            self.navigationLbl.text = "Privacy Policy"

        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
    }
    
}
