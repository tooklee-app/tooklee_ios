//
//  EmailOTPVerifyVC.swift
//  TookLee
//
//  Created by Nanak on 05/04/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class EmailOTPVerifyVC: BaseVc {

    // MARK: Variables
    var OTP: String = ""
    var email:String = ""
    var token:String!
    var timer : Timer!
    var count = 60
    var otpVarifyState : EnterEmailState = .varifyEmail
    
    // MARK: IBOutlets
    
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var continiueBtn: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var resendBtn: UIButton!
    
    @IBOutlet weak var bubbleView: UIView!
    
    @IBOutlet weak var firstDigitTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var secondDigitTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var thirdDigitTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var fourthDigitTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var enterOTPLabel: UILabel!
    
    
    // MARK: View controller life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.firstDigitTextField.text = nil
        self.secondDigitTextField.text = nil
        self.thirdDigitTextField.text = nil
        self.fourthDigitTextField.text = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private Methods
    
    func initialSetup() {
        
        if self.otpVarifyState != .varifyEmail{
            self.bubbleView.isHidden = true
        }
        self.view.applyGradient()

        self.setContiniueBtnStatus()
        self.firstDigitTextField.textAlignment = .center
        self.secondDigitTextField.textAlignment = .center
        self.thirdDigitTextField.textAlignment = .center
        self.fourthDigitTextField.textAlignment = .center
        self.firstDigitTextField.delegate = self
        self.secondDigitTextField.delegate = self
        self.thirdDigitTextField.delegate = self
        self.fourthDigitTextField.delegate = self
        self.popUpView.layer.cornerRadius = 3
        
        self.firstDigitTextField.addTarget(self, action: #selector(self.textfieldTextChanged(_:)), for: .editingChanged)
        self.secondDigitTextField.addTarget(self, action: #selector(self.textfieldTextChanged(_:)), for: .editingChanged)
        self.fourthDigitTextField.addTarget(self, action: #selector(self.textfieldTextChanged(_:)), for: .editingChanged)
        self.thirdDigitTextField.addTarget(self, action: #selector(self.textfieldTextChanged(_:)), for: .editingChanged)

        CommonFunction.setButtonState(false, sender: self.resendBtn, color : AppColors.appThemeFadeColor)
        self.startTimer()
        self.resendBtn.setTitle(StringConstants.RESEND_MSG.localized, for: .normal)

        self.enterOTPLabel.text = StringConstants.OTP_EMAIL_MSG.localized
    }
    
    
    
    
    func setContiniueBtnStatus(){
        
        self.OTP = self.firstDigitTextField.text! + self.secondDigitTextField.text! + self.thirdDigitTextField.text! + self.fourthDigitTextField.text!
        
        if isAllFieldsVerified() {
            CommonFunction.setButtonState(true, sender: self.continiueBtn, color: AppColors.continiueBtnColor)
        }else{
            
            CommonFunction.setButtonState(false, sender: self.continiueBtn, color: AppColors.continiueBtnFadeColor)
        }
    }

    
    @objc private func textfieldTextChanged(_ sender : SkyFloatingLabelTextField){
        
        self.setContiniueBtnStatus()
    }
    
    // Makes user enter only one digit in text field
    // Method to move to previuos textfield
    @objc func setPreviousResponder(_ textField: UITextField) {
        
        if textField === self.secondDigitTextField {
            
            _ = self.firstDigitTextField.becomeFirstResponder()
            
        } else if textField === self.thirdDigitTextField {
            
            _ = self.secondDigitTextField.becomeFirstResponder()
            
        } else if textField === self.fourthDigitTextField {
            
            _ = self.thirdDigitTextField.becomeFirstResponder()
            
        }
        
    }
    
    // Method to move to next textfield
    @objc func setNextResponder(_ textField: UITextField) {
        
        if textField === self.firstDigitTextField {
            
            _ = self.secondDigitTextField.becomeFirstResponder()
            
        } else if textField === self.secondDigitTextField {
            
            _ = self.thirdDigitTextField.becomeFirstResponder()
            
        } else if textField === self.thirdDigitTextField {
            
            _ = self.fourthDigitTextField.becomeFirstResponder()
            
        } else if textField === self.fourthDigitTextField {
            
            _ = self.fourthDigitTextField.resignFirstResponder()
            
        }
        
    }
    // Check is all field details are valid
    func isAllFieldsVerified() -> Bool {
        
        if self.OTP.isEmpty {
            
            return false
            
        } else if self.OTP.count < 4 {
            
            return false
        }
        
        return true
    }
    
    // MARK: IBActions
    @IBAction func backBtnTap(_ sender: UIButton) {
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
    }
    
    @IBAction func continiueBtnTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        // NavigationManager.moveToCreateProfile()
        self.OTP = self.firstDigitTextField.text! + self.secondDigitTextField.text! + self.thirdDigitTextField.text! + self.fourthDigitTextField.text!
        
        if isAllFieldsVerified() {
            
            self.verifyOTP()
            
        }
        
    }
    
    @IBAction func resendBtnTapped(_ sender: UIButton) {
        self.resendBtn.isEnabled = false
        self.view.endEditing(true)
        self.firstDigitTextField.text = ""
        self.secondDigitTextField.text = ""
        self.thirdDigitTextField.text = ""
        self.fourthDigitTextField.text = ""
        CommonFunction.setButtonState(false, sender: self.resendBtn, color : AppColors.appThemeFadeColor)
        self.startTimer()
        self.sendOtpOnEmail()
    }
    
    private func sendOtpOnEmail(){
        
        var params = JSONDictionary()
        
        params["email"] = self.email
        
        WebServices.sendOTPOnEmailAPI(parameters: params, webServiceSuccess: { (success, msg, json) in
            
            if success{
                
                showToastWithMessage(msg)

            }else{
                
                showToastWithMessage(msg)
            }
            
        }) { (err) -> (Void) in
            
        }
    }

    func startTimer(){
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)

    }
    
    @objc func update() {
        
        if(count > 0){
            
            self.count = self.count - 1
            
        }else{
            self.count = 60
            self.timer.invalidate()
            CommonFunction.setButtonState(true, sender: self.resendBtn, color : AppColors.appThemeColor)

        }
    }

    func verifyOTP() {
        
        var params = JSONDictionary()
        
        params["is_email_verification"] = 1
        
        params["otp"] = self.OTP
        
        WebServices.verifyOTP(parameters: params, webServiceSuccess: { (success, msg, json) in
            if success{
                self.timer.invalidate()
                self.moveToRightPlace()
            }else{
                
                showToastWithMessage(msg)
            }
        }) { (err) -> (Void) in
            
        }
        
    }
    
    
    private func moveToRightPlace(){
        
        switch self.otpVarifyState {
            
        case .varifyEmail:
            
            NavigationManager.movetoSelectPaymentType()

        case .changePhone:
            
            NavigationManager.moveToLoginWithPhone(.changePhone)
            
        }
    }
    
}

// MARK: Text field delegate life cycle methods

extension EmailOTPVerifyVC: UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !(textField.text!.isEmpty) {
            
            textField.text = ""
        }
        
        if (string.count == 0) && (range.length > 0)  {
            
            self.perform(#selector(self.setPreviousResponder(_:)), with: textField, afterDelay: 0.2)
            
        } else {
            
            self.perform(#selector(self.setNextResponder(_:)), with: textField, afterDelay: 0.1)
        }
        
        return true
    }}
