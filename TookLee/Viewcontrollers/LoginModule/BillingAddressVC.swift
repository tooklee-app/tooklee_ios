//
//  BillingAddressVC.swift
//  TookLee
//
//  Created by Nanak on 28/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class BillingAddressVC: UIViewController {
    
    @IBOutlet weak var addressTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    private func initialSetup(){
        
        self.addressTableView.delegate = self
        self.addressTableView.dataSource = self
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func continueTap(_ sender: UIButton){
        
        let obj = AddServicesVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(obj, animated: true)
    }
}


extension BillingAddressVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 6{
            return 120
        }else{
            return 85
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContinueBtnCell", for: indexPath) as! ContinueBtnCell
            cell.continueBtn.addTarget(self, action: #selector(self.continueTap(_:)), for: .touchUpInside)
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingAddressCell", for: indexPath) as! BillingAddressCell
            return cell
        }
    }
}

class BillingAddressCell: UITableViewCell {
    
    @IBOutlet weak var addressLbl: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

class ContinueBtnCell: UITableViewCell {
    
    @IBOutlet weak var continueBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
