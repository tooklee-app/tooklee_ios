//
//  LoginWithPhoneVC.swift
//  TookLee
//
//  Created by Nanak on 24/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit
import TTTAttributedLabel


class LoginWithPhoneVC: BaseVc {

    //MARK:- IBOutlets
    //MARK:- ===================================

    @IBOutlet weak var countryCodeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var loginBtn: TTTAttributedLabel!
    
    
    var enterPhoneState : EnterPhoneState = .signIn
    var maxNo = 9
    var minNo = 9
    
    //MARK:- Life cycle
    //MARK:- ===================================

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    //MARK:- Private Methods
    //MARK:- ===================================

    private func initialSetup(){
        
        self.view.applyGradient()

        CommonFunction.setButtonState(false, sender: self.continueBtn, color: AppColors.continiueBtnFadeColor)
        self.continueBtn.cornerRadius = self.continueBtn.bounds.height / 2
        self.countryCodeTextField.delegate = self
        self.phoneNumberTextField.delegate = self
        self.phoneNumberTextField.keyboardType = .numberPad
        self.countryCodeTextField.textAlignment = .center
        if self.enterPhoneState != .signUp{
            self.bubbleView.isHidden = true
        }else{
            self.bubbleView.isHidden = false
        }
        self.loginBtn.isHidden = true
        self.phoneNumberTextField.setPlaceHolder(with: "Phone Number", with: AppColors.continiueBtnFadeColor)

        self.loginBtn.delegate = self
        self.loginBtn.text = "Would you like to Sign In?"
        let str:NSString = self.loginBtn.text! as NSString
        let range : NSRange = str.range(of: "Would you like to Sign In?")
        
        self.loginBtn.addLink(to: NSURL(string: "abc")! as URL!, with: range)

        
    }
    
    private func validatePhone() -> Bool{
        
        if self.phoneNumberTextField.hasText{
            return true
        }else{
            return false
        }
    }
    
    private func makeParams(){
        
        if self.validatePhone(){
            
            var params : JSONDictionary = [:]
            params["country_code"] = self.countryCodeTextField.text
            params["phone"] = self.phoneNumberTextField.text
            
            if self.enterPhoneState == .signUp{
                
                self.hitweberviceForRegister(with : params)
                
            }else if self.enterPhoneState == .signIn{
                
                self.hitweberviceForSignIn(with : params)
                
            }else if self.enterPhoneState == .changePhone{
                
                self.hitweberviceForChangePhone(with : params)
                
            }
        }

    }
    
    
    //MARK:- IBActions
    //MARK:- ===================================

    @IBAction func continuebtnTap(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.makeParams()
    }
    
    @IBAction func backBtnTapp(_ sender: UIButton) {
        
        sharedAppDelegate.parentNavigationController.popViewController(animated: true)
    }
    
    @IBAction func phoneTextChanged(_ sender: SkyFloatingLabelTextField) {

        self.loginBtn.isHidden = true
        
        if sender.text!.count >= self.minNo{
            
            CommonFunction.setButtonState(true, sender: self.continueBtn, color: AppColors.continiueBtnColor)
        }else{
            CommonFunction.setButtonState(false, sender: self.continueBtn, color: AppColors.continiueBtnFadeColor)

        }
    }
    
    @IBAction func clickHereBtnTap(_ sender: UIButton) {
        NavigationManager.moveToEnterEmail(with: .changePhone)
    }
}


//MARK:- Setcountry code delegate
//MARK:- ===================================

extension LoginWithPhoneVC{
    
    
    private func hitweberviceForSignIn(with params : JSONDictionary){

        WebServices.signInAPI(parameters: params, webServiceSuccess: { (success, msg, json) in
            
            if success{
                
                let token = json["token"].stringValue
                AppUserDefaults.save(value: token, forKey: .Accesstoken)
                NavigationManager.moveToVarifyPhoneOtp(otpState: self.enterPhoneState ,token: token, code: self.countryCodeTextField.text!, phone: self.phoneNumberTextField.text!)

            }else{
                
                showToastWithMessage(msg)
            }
        }) { (err) -> (Void) in
            
        }
    }
    
    private func hitweberviceForRegister(with params : JSONDictionary){
        
        WebServices.signUpAPI(parameters: params, webServiceSuccess: { (code, msg, json) in
            
            if code == error_codes.success{
                
                let token = json["token"].stringValue
                AppUserDefaults.save(value: token, forKey: .Accesstoken)
                NavigationManager.moveToVarifyPhoneOtp(otpState: self.enterPhoneState ,token: token, code: self.countryCodeTextField.text!, phone: self.phoneNumberTextField.text!)

            }else if code == error_codes.alreadyRegistered{
                
                showToastWithMessage(msg)
                
                self.loginBtn.isHidden = false
                
            }else{
                
                showToastWithMessage(msg)

            }
        }) { (err) -> (Void) in
            
        }
    }
    
    
    private func hitweberviceForChangePhone(with params : JSONDictionary){
        
        WebServices.updatePhone(parameters: params, webServiceSuccess: { (success, msg, json) in
            
            if success{
                
                let token = json["token"].stringValue
                AppUserDefaults.save(value: token, forKey: .Accesstoken)
                NavigationManager.moveToVarifyPhoneOtp(otpState: self.enterPhoneState ,token: token, code: self.countryCodeTextField.text!, phone: self.phoneNumberTextField.text!)

            }
        }) { (err) -> (Void) in
            
        }
    }
}

//MARK:- TTTAtributedLbel Delegate method
//MARK:- ==============================

extension LoginWithPhoneVC : TTTAttributedLabelDelegate{
    // attriuted label didselect method
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        self.enterPhoneState = .signIn
        self.makeParams()
        
    }
    
}

//MARK:- Setcountry code delegate
//MARK:- ===================================

extension LoginWithPhoneVC: SetContryCodeDelegate{
    
    func setCountryCode(country_info: JSONDictionary) {
        guard let code = country_info["CountryCode"] else{return}
        self.countryCodeTextField.text = "+\(code)"
//        self.userDataDict["countrycode"] = "+\(code)"
//        guard let img = country_info["CountryFlag"] as? String else{return}
//        self.countryFlag.image = UIImage(named: img)
        guard let maxNo = country_info["Max NSN"] as? Int else{return}
        self.maxNo = maxNo
        guard let minNo = country_info["Min NSN"] as? Int else{return}
        self.minNo = minNo
        
    }
}


//MARK:- UITextfield delegate
//MARK:- ===================================


extension LoginWithPhoneVC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField === self.countryCodeTextField{
            
            let obj = CountryCodeVC.instantiate(fromAppStoryboard: .Main)
            obj.delegate = self
            self.navigationController?.pushViewController(obj, animated: true)
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && string == " "{
            return false
        }
        
        return self.validateInfo(textField: textField, string: string, range: range)
        

    }
    
    
    func validateInfo(textField :UITextField,string : String,range:NSRange) -> Bool {
        
        if range.length == 1 {
            
            return true
            
        }
        
            if let _ = textField.text {
                
                if textField.text!.count  >= self.maxNo {
                    self.view.endEditing(true)
                    return false
                    
                } else {
                    
                    return true
                    
                }
            }
        
        return true
    }
}
