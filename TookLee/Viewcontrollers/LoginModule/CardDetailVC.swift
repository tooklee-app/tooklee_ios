//
//  CardDetailVC.swift
//  TookLee
//
//  Created by Nanak on 26/01/18.
//  Copyright © 2018 Nanak. All rights reserved.
//

import UIKit

class CardDetailVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var cardNoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cvvTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var expiryDateTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var continiueBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textFieldEditind(_ sender: SkyFloatingLabelTextField) {
        
        
    }
    
    @IBAction func continueTap(_ sender: UIButton) {
        NavigationManager.gotoHome()
    }
    
}
